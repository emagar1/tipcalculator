package edu.towson.cosc431.Magarin.TipCalculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // set the button listener
        calculateButton.setOnClickListener {calculateTip()}
    }

    /**
     * Handler for the calc button click
     */
    fun calculateTip() {

        // get the input bill amount from the edit text
        val inputString = billAmount.editableText.toString()

        // check the id of the checked radio button
        val checkedId = radioGroup.checkedRadioButtonId

        // set the percent on different checked id
        val percent = when(checkedId) {
            RdB10.id -> 10.0
            RdB20.id -> 20.0
            RdB30.id -> 30.0
            else -> throw Exception("Unexpected value!")
        }

        // calculation
        val result = calculation(percent, inputString)

        // set the result textview
        when(result) {
            null -> resultTextView.text = " Error Enter amount"//nothing entered
            else -> resultTextView.text = result //if not wrong the result is produced from above
        }
    }

    /**
     * A function to do the calculation
     */
    fun calculation(percent: Double, strValue: String): String? {
        try {
            val amount = strValue.toDouble()
            val calculatedTip = (percent * amount)/100
            val total = amount + calculatedTip //the total amount, adding tip

            //the string that is returned in the result text box
            return "Your total is $${String.format("%.2f", total)} " +
                    "and your tip amount is $${String.format("%.2f", calculatedTip)}"

        } catch (e: NumberFormatException) {
            return null
        }
    }

}
